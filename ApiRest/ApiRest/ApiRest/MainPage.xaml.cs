﻿using ApiRest.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ApiRest
{
	public partial class MainPage : ContentPage
    {
        private const string Url = "http://jonmidpruebanode.herokuapp.com/users";
        private readonly HttpClient client = new HttpClient();
        private ObservableCollection<User> _user;

		public MainPage()
		{
			InitializeComponent();
		}

        async public void ListData()
        {
            string content = await client.GetStringAsync(Url);
            List<User> users = JsonConvert.DeserializeObject<List<User>>(content);
           _user = new ObservableCollection<User>(users);
            listviewUsers.ItemsSource = _user;
        }
        async public void CreateData()

        {
            User user = new User(){
                Name = "Carlos"
            };

            var json = JsonConvert.SerializeObject(user);
            var content = new StringContent(json, Encoding.UTF8, "Application / json");
            HttpResponseMessage response = null;
            response = await client.PostAsync(Url, content);

           
        }
        public void ClickListData(Object sender, EventArgs e)
        {
            ListData();
        }
        // se ejecuta cada que esta activa la pantalla
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ListData();
        }
    }
}
